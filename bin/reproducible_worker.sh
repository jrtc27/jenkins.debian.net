#!/bin/bash
# vim: set noexpandtab:

# Copyright © 2017-2018 Holger Levsen (holger@layer-acht.org)
#           © 2018-2024 Mattia Rizolo <mattia@debian.org>
# released under the GPLv2

set -e

# exit status:
# 8: worker undefined
# 9: lockfile present
# 10: this script is already running

WORKER_NAME=${1:?worker name}

# normally defined by jenkins and used by reproducible_common.sh
JENKINS_URL=https://jenkins.debian.net

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

#
# this function defines which builds take place on which nodes
#
choose_nodes() {
	line=$(grep "$1 " /srv/jenkins/bin/reproducible_build_workers|grep -v '^#')
	if [ -n "$line" ]; then
		NODE1=$(echo "$line"|awk '{print $2}')
		NODE2=$(echo "$line"|awk '{print $3}')
	else
		NODE1=undefined
	fi
}


notify_log_of_failure() {
	local PKG_SUITE="$1"
	tee -a /var/log/jenkins/reproducible-builder-errors.log <<-END

		$(date -u) - $WORKER_NAME/$BUILD_ID for $PKG_SUITE exited with error code: $RETCODE
		Check out $BUILD_URL
		END
}

main_loop() {
	choose_nodes "$WORKER_NAME"
	# check inside the loop, so that the worker can be disabled by just changing
	# the file without stopping everything.
	if [ "$NODE1" = "undefined" ]; then
		exit 8
	fi
	LOCKFILE="/var/lib/jenkins/NO-RB-BUILDERS-PLEASE"
	if [ -f "$LOCKFILE" ]; then
		echo "The lockfile $LOCKFILE is present, thus stopping this"
		exit 9
	fi
	# try systemctl twice, but only output and thus log the 2nd attempt…
	RUNNING=$(systemctl --user show -P SubState "$SERVICE")
	if [ "$RUNNING" != "running" ] ; then
		# sometimes systemctl requests time out… handle that gracefully
		sleep 23
		RUNNING=$(systemctl --user show -P SubState "$SERVICE")
		if [ "$RUNNING" != "running" ] ; then
			echo "$(date --utc) - '$SERVICE' not running, thus stopping this."
			sleep 42.1337m
			exit 1
		fi
	fi
	if [ -f "$JENKINS_OFFLINE_LIST" ]; then
		for n in "$NODE1" "$NODE2"; do
			if grep -q "$n" "$JENKINS_OFFLINE_LIST"; then
				echo "$n is currently marked as offline, sleeping an hour before trying again."
				sleep 60.1337m
				return
			fi
		done
	fi


	# sleep up to 2.3 seconds to help randomize startup
	# (additionally to the random sleep reproducible_build.sh does anyway)
	/bin/sleep "$(echo "scale=1 ; $(shuf -i 1-23 -n 1)/10" | bc )"

	#
	# increment BUILD_ID
	#
	BUILD_BASE=/var/lib/jenkins/userContent/reproducible/debian/build_service/$WORKER_NAME
	if [ -L "$BUILD_BASE"/latest ]; then
		LAST_ID=$(basename "$( readlink -m "$BUILD_BASE/latest" )")
	else
		LAST_ID=0
	fi
	BUILD_ID=$(( LAST_ID + 1 ))
	mkdir -p "$BUILD_BASE/$BUILD_ID"
	rm -f "$BUILD_BASE/latest"
	ln -sf "$BUILD_ID" "$BUILD_BASE/latest"

	#
	# prepare variables for export
	#
	export BUILD_URL=https://jenkins.debian.net/userContent/reproducible/debian/build_service/$WORKER_NAME/$BUILD_ID/
	export BUILD_ID=$BUILD_ID
	export JOB_NAME="reproducible_builder_$WORKER_NAME"

	#
	# actually run reproducible_build.sh
	#
	echo
	echo "================================================================================================"
	echo "$(date --utc) - running build #$BUILD_ID for $WORKER_NAME on $NODE1 and $NODE2."
	echo "                               see https://tests.reproducible-builds.org/cgi-bin/nph-logwatch?$WORKER_NAME/$BUILD_ID"
	echo "================================================================================================"
	echo
	RETCODE=0
	systemd-run --user --send-sighup --collect --pipe --wait \
		--slice=rb-build.slice -u "rb-build-$WORKER_NAME-$BUILD_ID" \
		-E BUILD_ID -E JOB_NAME -E BUILD_URL \
		/srv/jenkins/bin/reproducible_build.sh "$NODE1" "$NODE2" \
		> "$BUILD_BASE/$BUILD_ID/console.log" 2>&1 || RETCODE=$?

	if [ "$RETCODE" -ne 0 ] ; then
		local FAILED
		FAILED="$(grep '^Initialising reproducibly build' "$BUILD_BASE/$BUILD_ID/console.log" | cut -d ' ' -f5-7)"
		 notify_log_of_failure "$FAILED"
	fi
	gzip "$BUILD_BASE/$BUILD_ID/console.log" || true
	echo
}

#
# check if we really should be running
#
PGREP_OUT=$(pgrep -f "reproducible_worker.sh $1\$"|grep -v $PPID||true)
if [ -n "$PGREP_OUT" ]; then
	echo "$(date --utc) - 'reproducible_worker.sh $1' already running (I'm $$, father is $PPID):"
	pgrep -f -a "reproducible_worker.sh $1\$"
	echo "$(date --utc) - stopping the current process."
	exit 10
fi

#
# main
#

if [ -z "${XDG_RUNTIME_DIR:-}" ]; then
	XDG_RUNTIME_DIR="/run/user/$UID"
	export XDG_RUNTIME_DIR
fi
SERVICE="reproducible_build@${WORKER_NAME}.service"

while true ; do
	main_loop
done
